import { html } from "lit-element";

const linksBootstrap = html`
  <link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
    crossorigin="anonymous"
  />
  <link
    href="https://bootswatch.com/5/yeti/bootstrap.min.css"
    rel="stylesheet"
  />
`;

export { linksBootstrap };
