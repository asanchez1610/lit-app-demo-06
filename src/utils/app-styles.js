import { css } from "lit-element";

export default css`

.main {
    margin: 15px;
    width: calc(100% - 62px);
    display: flex;
    padding: 15px;
    background-color: white;
    border: 1px solid #e1e1e1;
    min-height: calc(100vh - 115px);
}

.section-form {
    width: 35%;
}

.section-list {
    width: 65%;
}

h2 {
    margin: 0;
    font-size: 1.2em;
    font-weight: normal;
    padding-bottom: 5px;
    border-bottom: 1px solid #e1e1e1;
}

@media only screen and (max-width: 720px)  {
    .section-form {
        width: 100%;
     }

    .section-list {
        width: 100%;
    }
    .main {
        flex-direction: column;
    }
}


`;