import { BaseElement } from "@bbva-commons-web-components/cells-base-elements";
import { Service} from './Service';
class ServiceDm extends BaseElement {

    constructor() {
        super();
    }

    get service() {
        return new Service();
    }

    async createPerson(person) {
        return await this.service.createPersona(person);
    }

    async listPerson() {
        return await this.service.listPersona();
    }
    
}
customElements.define('service-dm', ServiceDm);