export class Service {
  constructor() {
    this.server = "http://scrum-quantum-labs.com:3000";
    this.endPoints = {
        base: "api/persons"
    }
  }

  async createPersona(person) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let settings = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(person)
    };
    let request = new Request(`${this.server}/${this.endPoints.base}`, settings);
    let response = await fetch(request).then(response => response.json());
    return response;
  }

  async listPersona() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let settings = {
      method: "GET",
      headers: headers
    };
    let request = new Request(`${this.server}/${this.endPoints.base}`, settings);
    let response = await fetch(request).then(response => response.json());
    return response;
  }

}
