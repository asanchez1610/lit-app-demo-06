import { html, css } from "lit-element";
import "./components/header-app/header-app";
import "./components/form-persona-app/form-persona-app";
import "./components/list-persona-app/list-persona-app"
import stylesApp from "./utils/app-styles";
import './dms/service-dm/service-dm';
import { BaseElement } from "@bbva-commons-web-components/cells-base-elements";
import '@polymer/paper-toast/paper-toast';

export class LitAppDemo06 extends BaseElement {
  static get properties() {
    return {};
  }

  static get styles() {
    return [stylesApp,
    css`
      paper-toast {
            --paper-toast-background-color: green;
            --paper-toast-color: white;
        }
    `
    ];
  }

  constructor() {
    super();
  }

  firstUpdated() {
    this.loadPersons();
  }

  get serviceDm() {
    return this.element('service-dm');
  }

  register({detail}) {
    console.log('register', detail);
    this.serviceDm.createPerson(detail);
    this.element('paper-toast').open();
    this.loadPersons();
  }

  async loadPersons() {
    const persons = await this.serviceDm.listPerson(); 
    this.element('list-persona-app').persons = persons;
  }

  render() {
    return html`
      <header>
        <header-app header-title="App de Personas"></header-app>
      </header>

      <main class="main">
        <section class="section-form">
          <h2>Registro de Personas</h2>
          <form-persona-app @on-register-persona="${this.register}" ></form-persona-app>
        </section>
        <section class="section-list">
          <list-persona-app></list-persona-app>
        </section>
      </main>
      <service-dm></service-dm>
      <paper-toast text="La persona ha sido registrada de forma correcta." ></paper-toast>
      
    `;
  }
}
