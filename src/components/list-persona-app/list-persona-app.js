import { BaseElement } from "@bbva-commons-web-components/cells-base-elements";
import { html, css } from "lit-element";
import { linksBootstrap } from '../../utils/links-css-bootstrap';

class ListPersonaApp extends BaseElement {
  static get styles() {
    return css`
        main {
            padding: 5px 40px;
        }
    `;
  }

  static get properties() {
    return { persons: Array };
  }

  constructor() {
      super();
      this.persons = [];
  }

  render() {
    return html`
      ${linksBootstrap}

      <main>
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Nombre completo</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Email</th>
              </tr>
            </thead>
            <tbody>
              ${this.persons.map(
                  (item) => 
                      html`
                          <tr>
                                <td>${item.nombres}</td>
                                <td>${item.telefono}</td>
                                <td>${item.email}</td>
                          </tr>
                      `
              )}
            </tbody>
          </table>
      </main>
    `;
  }
}
customElements.define("list-persona-app", ListPersonaApp);
