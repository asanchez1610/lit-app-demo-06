import { html, css } from 'lit-element';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement';
import { linksBootstrap } from '../../utils/links-css-bootstrap';
import '@polymer/paper-toast/paper-toast';

class FormPersonaApp extends BaseElement { 

    static get styles() {
        return css`
        .actions-butttons {
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 5px;
            width: 100%;
        }

        .actions-butttons *{
            margin: 5px;
        }

        #toast {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
        }

        `
    }

    static get properties() {
        return { 
            persona: Object
        };
    }

    constructor() {
        super();
        this.persona = {};
        this.addListeners();
    }

    async addListeners() {
        await this.updateComplete;
        const inputs = this.elementsAll('input');
        inputs.forEach(input => {
            input.addEventListener('input',({target}) => this.buildPersona(target));
        });
    }

    registrar() {
        if(this.validateForm()) {
            this.dispatch('on-register-persona', this.persona);
            this.reset();
        } else {
            this.element('#toast').open();
        }
    }

    reset() {
        this.persona = {};
        this.requestUpdate();
    }

    buildPersona(input) {
        let tempPersona = { ...this.persona };
        tempPersona[input.name] = input.value;
        this.persona = { ...tempPersona };
    }

    validateForm() {
        const inputs = this.elementsAll('input');
        let errors = 0;
        inputs.forEach(input => {
           if(this.isBlank(input.value)) {
               errors = errors + 1;
               input.classList.add('is-invalid');
           } else {
            input.classList.remove('is-invalid');
           }
        });
        return errors === 0;
    }

    render() {
        return html`
        ${linksBootstrap}

        <div class="form-group">
            <label for="name" class="form-label mt-4">Nombre completo</label>
            <input name="nombres" .value = "${this.extract(this.persona, 'nombres', '')}" class="form-control" id="name">
        </div>
        <div class="form-group">
            <label for="telefono" class="form-label mt-4">Teléfono</label>
            <input name="telefono" class="form-control" .value = "${this.extract(this.persona, 'telefono', '')}" id="telefono">
        </div>

        <div class="form-group">
            <label for="email" class="form-label mt-4">Correo electrónico</label>
            <input name="email" class="form-control" .value = "${this.extract(this.persona, 'email', '')}" id="email">
        </div>

        <div class="actions-butttons">
            <button class="btn btn-warning" @click="${this.reset}" >Cancelar</button>
            <button class="btn btn-primary" @click="${this.registrar}" >Registrar</button>
        </div>

        <paper-toast id="toast" text="Verifique los campos en rojo!"></paper-toast>    

       `
    }
}
customElements.define('form-persona-app', FormPersonaApp);