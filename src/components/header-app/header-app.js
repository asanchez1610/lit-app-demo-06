import { LitElement, html, css } from "lit-element";
import { linksBootstrap } from '../../utils/links-css-bootstrap';

class HeaderApp extends LitElement {
  static get styles() {
    return css``;
  }

  constructor() {
      super();
      this.title = 'My App';
  }

  static get properties() {
    return {
        title: { 
            type: String,
            attribute: 'header-title'
        }
    };
  }

  render() {
    return html`
      ${linksBootstrap}
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">${this.title}</a>
        </div>
      </nav>
    `;
  }
}
customElements.define("header-app", HeaderApp);
